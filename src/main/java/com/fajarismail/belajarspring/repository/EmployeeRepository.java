package com.fajarismail.belajarspring.repository;

import com.fajarismail.belajarspring.model.Employee;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findByName(String name);

    //SELECT * FROM table WHERE name"ismail" AND location="Bandung"
    List<Employee> findByNameAndLocation(String name, String location);

    //SELECT * FROM table WHERE name LIKE"ism"
    List<Employee> findByNameContaining(String keyword, Sort sort);

    @Query("FROM Employee WHERE name = :name OR location = :location")
    List<Employee> findByNameOrLocation(String name, String location);

    @Transactional
    @Modifying
    @Query("DELETE FROM Employee WHERE name = :name")
    Integer deleteEmployeeByName(String name);
}
